;; SPDX-FileCopyrightText: 2020 pukkamustard <pukkamustard@posteo.net>
;;
;; SPDX-License-Identifier: AGPL-3.0-or-later
(use-modules
 (guix packages)
 (guix git-download)
 (guix build-system gnu)
 ((guix licenses) #:prefix license:)
 (gnu packages guile)
 (gnu packages gettext)
 (gnu packages guile-xyz))

(package
  (name "openengiadina.net")
  (version "0.0")
  (source #f)
  (build-system gnu-build-system)
  (inputs
   `(("gettext" ,gettext)
     ("haunt" ,haunt)))
  (synopsis #f)
  (description #f)
  (home-page "https://openengiadina.net")
  (license license:gpl3+))
