# SPDX-FileCopyrightText: 2020 pukkamustard <pukkamustard@posteo.net>
#
# SPDX-License-Identifier: CC0-1.0
#
#+TITLE: openengiadina.net website

This repository contains source code for the [[https://openengiadina.net]] website.

* Building

To build the site to the ~site~ folder:

#+BEGIN_SRC shell
guix environment -l guix.scm -- make
#+END_SRC

To start a live reloading server:

#+BEGIN_SRC shell
guix environment -l guix.scm -- make serve
#+END_SRC

The site can now be viewed at [[http://localhost:8080/]].

* Translations

The openEngiadina website can be translated to different languages.

Translations are in the ~po/*.po~ files and will be used when building the site.

** Extract strings to translate

Strings that can be translated (in ~G_~ forms) can be extracted into the PO Template with ~make xgettext~. This will also update ~.po~ files with ~msgmerge~.

** Adding a new language

Currently a manual process.

1. Create a new ~.po~ file:
    #+BEGIN_SRC shell
    cd po
    msginit -l rm.utf8 --no-translator
    #+END_SRC
1. Add language in [[./open-engiadina/website/i18n.scm][~i18n.scm~]].
2. Add language to Makefile (this needs to be improved)
