#  SPDX-FileCopyrightText: 2020 pukkamustard <pukkamustard@posteo.net>
#
#  SPDX-License-Identifier: AGPL-3.0-or-later

PWD=$(dir $(realpath $(firstword $(MAKEFILE_LIST))))
HAUNT=GUILE_LOAD_PATH=$(PWD):$(GUILE_LOAD_PATH) haunt

SCM_SRC=haunt.scm open-engiadina/website/*.scm

.PHONY: build
build: locale/de/LC_MESSAGES/openengiadina-website.mo
	@$(HAUNT) build

.PHONY: xgettext
xgettext: po/openengiadina-website.pot
	msgmerge --previous -U po/de.po $<

po/openengiadina-website.pot: $(SCM_SRC)
	mkdir -p po
	xgettext \
		-o po/openengiadina-website.pot \
		--keyword="G_" \
		--add-comments="TRANSLATORS" \
		--from-code=UTF-8 \
		--copyright-holder="openEngiadina Authors <https://openengiadina.net>" \
		--package-name="openengiadina-website" \
		--msgid-bugs-address="bugs@openengiadina.net" \
		$(SCM_SRC)

locale/de/LC_MESSAGES/openengiadina-website.mo: po/de.po
	mkdir -p locale/de/LC_MESSAGES
	msgfmt $< -o $@

serve: haunt.scm
	@$(HAUNT) serve -w

publish:
	rsync -rav -e ssh site/ qfwfq:public_html/openengiadina.net/ --delete
