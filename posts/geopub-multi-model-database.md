title: GeoPub: A Multi-Model Database
date: 2022-07-01 12:00
link: https://inqlab.net/2022-07-01-geopub-a-multi-model-database.html
---

We are happy to announce the release of GeoPub [v0.6.0](https://codeberg.org/openEngiadina/geopub/releases/tag/v0.6.0).

This adds support for geo-spatial and full-text search indexing. Read the [post](https://inqlab.net/2022-07-01-geopub-a-multi-model-database.html) for the ideas behind this. You can try it out on our [demo instance](https://geopub.openengiadina.net) (note that initial loading takes a long time). 
