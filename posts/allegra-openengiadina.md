title: Allegra openEngiadina!
date: 2020-03-21 12:00
link: https://inqlab.net/2020-03-21-allegra-openengiadina.html
---
We are very happy to announce that openEngiadina is now being supported by the NLNet Foundation.

An outline of the background, general ideas and next steps of the project.
