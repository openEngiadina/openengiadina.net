title: GeoPub: Datalog, RDF and IndexedDB
date: 2022-04-14 12:00
link: https://inqlab.net/2022-04-14-geopub-datalog-rdf-and-indexeddb.html
---

We are happy to announce the release of GeoPub [v0.5.0](https://codeberg.org/openEngiadina/geopub/releases/tag/v0.5.0).

Read the [post](https://inqlab.net/2022-04-14-geopub-datalog-rdf-and-indexeddb.html) for a description of the changes and try it out [here](https://geopub.openengiadina.net/) (Note that initial loading might take some time).
