title: Distributed Mutable Containers
date: 2020-10-19 12:00
link: http://purl.org/dmc
---
As part of the NLNet project we have been doing [research into data model and data storage](https://codeberg.org/openEngiadina/data-model).

We are happy to announce an initial version of [Distributed Mutable Container](http://purl.org/dmc), which defines mutable containers that can be used in offline-first and decentralized applications.

We intend to implement the containers in our [ActivityPub backend](https://codeberg.org/openEngiadina/cpub), allowing it to serve as a bridge between more conventional HTTP-based ActivityPub and more robust and decentralized protocols.
