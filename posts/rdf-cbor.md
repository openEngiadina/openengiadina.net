title: RDF/CBOR: A CBOR-based Serialization of RDF
date: 2022-08-19 12:00
link: https://openengiadina.codeberg.page/rdf-cbor/
---

In order to enable decentralized availablility of content we have been working towards a binary serialization of RDF that allows content-addressing: RDF/CBOR.

This is a update to previous work on content-addressable RDF.
