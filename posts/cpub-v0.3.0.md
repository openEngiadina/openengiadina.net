title: CPub: ActivityPub Client-to-Server Protocol
date: 2022-04-24 12:00
link: https://codeberg.org/openEngiadina/cpub/releases/tag/v0.3.0
---

We are happy to announce the release of CPub [v0.3.0](https://codeberg.org/openEngiadina/cpub/releases/tag/v0.3.0). This release includes support for the [ActivityPub Client-to-Server Protocol](https://www.w3.org/TR/activitypub/#client-to-server-interactions).

This is the final release of CPub as part of the openEngiadina project. We are now using the XMPP protocol that allows us to use existing server software (see [this post](https://inqlab.net/2021-11-12-openengiadina-from-activitypub-to-xmpp.html)).

A more detailed release announcement has been posted on [SocialHub](https://socialhub.activitypub.rocks/t/cpub-v0-3-0-activitypub-client-to-server-protocol/2393).
