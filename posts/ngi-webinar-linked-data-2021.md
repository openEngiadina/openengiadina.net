title: Next Generation Internet webinar on Linked Data
date: 2021-06-19 12:00
link: https://nlnet.nl/events/20210621/LinkedData/
---

Join us in the NGI webinar on Linked Data! 

We will be talking about how openEngiadina is using Linked Data and look forward to hearing about many other interesting projects working on Linked Data in various forms.
