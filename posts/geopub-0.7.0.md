title: GeoPub v0.7.0
date: 2022-09-20 12:00
link: https://codeberg.org/openEngiadina/geopub/releases/tag/v0.7.0
---

This release demonstrates core ideas developed as part of the openEngiadina project. 

A demo installation is available at https://geopub.openengiadina.net .

There are still some rough-edges and performance issues. But we hope the core ideas get through and might even inspire you.
