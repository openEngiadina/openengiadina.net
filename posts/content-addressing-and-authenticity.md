title: Content-addressing and Authenticity
date: 2020-06-11 12:00
link: https://openengiadina.gitlab.io/js-eris/index.html
---
As part of the NLNet project we have been doing [research into data model and data storage](https://codeberg.org/openEngiadina/data-model).

We are happy to announce initial [results](http://purl.org/eris) and a [demo](https://openengiadina.gitlab.io/js-eris/index.html)!

This enables [content-addressable RDF](http://purl.org/ca-rdf) and [ensuring authenticity](http://purl.org/signify) of RDF content, enabling offline-first and decentralized applications - things we are trying to build with openEngiadina.
