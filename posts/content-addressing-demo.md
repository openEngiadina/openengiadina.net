title: Demo: Federated Authentication and Content-addressing
date: 2020-08-12 12:00
link: https://socialhub.activitypub.rocks/t/demo-federated-authentication-and-content-addressing/874
---

We have another little demo showing how federated authentication might look and how previous work on content-addressing works.
