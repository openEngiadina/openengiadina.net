title: ActivityStreams over XMPP
date: 2022-01-17 12:00
link: https://inqlab.net/2022-01-17-activitystreams-over-xmpp.html
---

Another year, another small step. GeoPub now uses ActivityStreams over XMPP.

See also the release [v0.4.0](https://codeberg.org/openEngiadina/geopub/releases/tag/v0.4.0) and try it out [here](https://geopub.openengiadina.net/).

