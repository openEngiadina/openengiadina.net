title: From ActivityPub to XMPP
date: 2021-11-12 12:00
link: https://inqlab.net/2021-11-12-openengiadina-from-activitypub-to-xmpp.html
---

We are happy to announce another release of GeoPub ([v0.3.0](https://codeberg.org/openEngiadina/geopub/releases/tag/v0.3.0)). This release includes a major architectural change: XMPP instead of ActivityPub. Furthermore, this release supports geospatial data by including a map.
