;; SPDX-FileCopyrightText: 2020 pukkamustard <pukkamustard@posteo.net>
;;
;; SPDX-License-Identifier: AGPL-3.0-or-later

(define-module (open-engiadina website index)
  #:use-module (haunt post)
  #:use-module (haunt site)
  #:use-module (haunt page)
  #:use-module (haunt html)
  #:use-module (open-engiadina website layout)
  #:use-module (open-engiadina website i18n)
  #:use-module (srfi srfi-19)
  #:use-module (commonmark)
  #:export (translated-index-page-builder
            index-page-builder))

(define (read-commonmark file)
  (call-with-input-file file
    (lambda (port)
      (commonmark->sxml port))))

(define (date->string* date)
  (date->string date "~e.~m.~Y"))

(define (news-entry post)
  `((li ,(if (post-ref post 'link)
             `(a (@ (href ,(post-ref post 'link)))
                 ,(date->string* (post-date post))
                 " — "
                 ,(post-ref post 'title))
             (string-append
              (date->string* (post-date post))
              " — "
              (post-ref post 'title))))
    ,(post-sxml post)))

(define (news posts)

  `((h1
     ;; TRANSLATORS: News is currently only available in English. It might make sense to indicate that this section is only available in English (e.g. "Neuigkeiten (Englisch)")
     ,(G_ "News"))
    (ul
     (@ ("class" "news"))
     ,(map news-entry
           (posts/reverse-chronological posts)))
    (p (@ (class "right-align"))
       (a (@ (href "news.xml")) ,(G_ "Atom feed")))))

(define (supported-by)
  `((div (@ ("id" "supported-by"))
         (h1 ,(G_ "Acknowledgments"))

         (div (@ ("class" "row"))
              (a (@ ("href" "https://nlnet.nl/"))
                 (img (@ ("src" "/images/nlnet.svg"))))

              (a (@ ("href" "https://nlnet.nl/discovery"))
                 (img (@ ("src" "/images/NGI0_tag.svg")))))

         ;; (div (@ ("class" "row"))
         ;;      (a (@ ("href" "https://miaengiadina.ch/"))
         ;;         (img (@ ("src" "/images/mE_Logo_2132x440.png"))))

         ;;      (a (@ ("href" "https://scuol-zernez.engadin.com"))
         ;;         (img (@ ("src" "/images/quer_Eng_Scuol_Zernez_RGB_ohneNPR.png")))))

         ;; (div (@ ("class" "row"))
         ;;      (a (@ (href "https://ungleich.ch/"))
         ;;         (img (@ ("src" "/images/ungleich_logo.jpg")))))

	 )))

(define (translated-index-page-builder site posts)
  (map
   (lambda (lang)
     (setlocale LC_ALL (language-locale lang))
     (index-page-builder site posts (string-append (language-tag lang) "/index.html")))
   %languages))

(define* (index-page-builder site posts #:optional (out "index.html"))
  (make-page out
             (layout site
                     `( ;; ,(read-commonmark "pages/info.md")

                       (p ,(G_ "openEngiadina is developing a platform for open local knowledge."))

                       (section

                        (h3 ,(G_ "Open Local Knowledge"))

                        (p ,(G_ "Local knowledge consists of thousand of little pieces of information that describe the social, cultural and natural environment of an area. For example:"))

                        (div (@ ("class" "row"))
                             (div
                              (img (@ ("class" "icon") ("src" "/images/map-2-line.svg")))
                              (p ,(G_ "Geographic information")))

                             (div
                              (img (@ ("class" "icon") ("src" "/images/calendar-event-line.svg")))
                              (p ,(G_ "Social and cultural events")))

                             (div
                              (img (@ ("class" "icon") ("src" "/images/store-2-line.svg")))
                              (p ,(G_ "Organizations and businesses")))

                             (div
                              (img (@ ("class" "icon") ("src" "/images/traffic-light-line.svg")))
                              (p ,(G_ "Infrastructure status"))))

                        (p ,(G_ "We believe that local knowledge needs to be freely available for everyone to use and republish as they wish. Local knowledge needs to be open knowledge.")))


                       (section
                        (h3 ,(G_ "Semantic Social Network"))

                        (p ,(G_ "openEngiadina is an acknowledgment that data creation and curation is a social activity. We are developing a mashup between a knowledge base (like Wikipedia) and a social network using the XMPP protocol and the Semantic Web.")))

                       (section

                        (h3 ,(G_ "For whom and why?"))

                        (p ,(G_ "openEngiadina is being developed for small municipalities and local organizations."))

                        (p ,(G_ "We want to empower communities to manage their own data. The platform is Free Software, self-hostable and can interoperate with other instances."))

                        (div (@ ("class" "row"))
                             (div
                              (img (@ ("class" "icon") ("src" "/images/hand-heart-line.svg")))
                              (p ,(G_ "Free software")))

                             (div
                              (img (@ ("class" "icon") ("src" "/images/plant-line.svg")))
                              (p ,(G_ "Self-hostable")))

                             (div
                              (img (@ ("class" "icon") ("src" "/images/bubble-chart-line.svg")))
                              (p ,(G_ "Decentralized")))))


                       ,(news posts)

                       ,(supported-by)))

             sxml->html))

