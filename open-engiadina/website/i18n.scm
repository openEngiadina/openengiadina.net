;; SPDX-FileCopyrightText: 2020 pukkamustard <pukkamustard@posteo.net>
;;
;; SPDX-License-Identifier: AGPL-3.0-or-later

(define-module (open-engiadina website i18n)
  #:use-module (srfi srfi-9)
  #:export (G_
            %languages
            language-tag
            language-locale))

(define-record-type language
  (make-language tag locale)
  language?
  (tag language-tag)
  (locale language-locale))

(define %languages
  (list
   (make-language "en" "en_US.utf8")
   (make-language "de" "de_DE.utf8")))

(define %gettext-domain "openengiadina-website")

(define (G_ msg) (gettext msg %gettext-domain))

;; Initialization
(bindtextdomain %gettext-domain (string-append (getcwd) "/locale"))
(bind-textdomain-codeset %gettext-domain "UTF-8")
(textdomain %gettext-domain)
